package com.test.pokemon.data.entity

import com.google.gson.annotations.SerializedName

data class Pokemon(
    val id: Int,
    val name: String,
    val height: Int,
    val weight: Int,
    val species: NamedApiResource,
    val stats: List<PokemonStat>,
    val types: List<PokemonType>,
    val sprites: PokemonSprites
)

data class PokemonStat(
    val stat: NamedApiResource,
    val effort: Int,
    val baseStat: Int
)

data class PokemonType(
    val slot: Int,
    val type: NamedApiResource
)

data class PokemonSprites(
    val frontDefault: String?,
    val other: PokemonSpritesOther?
)

data class PokemonSpritesOther(
    @SerializedName("official-artwork")
    val officialArtwork: OfficialArtwork?
)

data class OfficialArtwork(
    val frontDefault: String?,
)

data class PokemonSpecies(
        val color: NamedApiResource,
        val flavorTextEntries: List<PokemonSpeciesFlavorText>
)

data class PokemonSpeciesFlavorText(
        val flavorText: String,
        val language: NamedApiResource,
        val version: NamedApiResource
)

