package com.test.pokemon.data.entity

data class NamedApiResource(
    val name: String,
    val url: String
)

data class NamedApiResourceList(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<NamedApiResource>
)