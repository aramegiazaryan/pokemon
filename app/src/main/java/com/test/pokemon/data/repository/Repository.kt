package com.test.pokemon.data.repository

import com.test.pokemon.data.entity.NamedApiResourceList
import com.test.pokemon.data.entity.Pokemon
import com.test.pokemon.data.entity.PokemonSpecies
import com.test.pokemon.data.repository.remote.PokemonApiDataSource

class Repository(private val pokemonApiDataSource: PokemonApiDataSource) {
    suspend fun getPokemonList(offset: Int, limit: Int): NamedApiResourceList =
            pokemonApiDataSource.getPokemonList(offset, limit)

    suspend fun getNextPokemonList(url: String): NamedApiResourceList =
            pokemonApiDataSource.getNextPokemonList(url)

    suspend fun getPokemon(url: String): Pokemon = pokemonApiDataSource.getPokemon(url)

    suspend fun getPokemon(id: Int): Pokemon = pokemonApiDataSource.getPokemon(id)

    suspend fun getPokemonSpecies(url: String): PokemonSpecies =
            pokemonApiDataSource.getPokemonSpecies(url)
}