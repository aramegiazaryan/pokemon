package com.test.pokemon.data.repository.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit

object ApiFactory {
    inline fun <reified T> create(
        baseUrl: String,
        client: OkHttpClient = OkHttpClient(),
        action: ((builder: Retrofit.Builder) -> Retrofit.Builder)
    ): T = action.invoke(Retrofit.Builder())
        .client(client)
        .baseUrl(baseUrl)
        .build()
        .create(T::class.java)
}