package com.test.pokemon.data.repository.remote

import com.test.pokemon.data.entity.NamedApiResourceList
import com.test.pokemon.data.entity.Pokemon
import com.test.pokemon.data.entity.PokemonSpecies
import retrofit2.http.*

interface PokemonApi {

    @GET("pokemon/")
    @Headers("Cacheable: true")
    suspend fun getPokemonList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): NamedApiResourceList

    @GET
    @Headers("Cacheable: true")
    suspend fun getNextPokemonList(
        @Url url: String,
    ): NamedApiResourceList

    @GET
    @Headers("Cacheable: true")
    suspend fun getPokemon(
        @Url url: String,
    ): Pokemon

    @GET("pokemon/{id}")
    @Headers("Cacheable: true")
    suspend fun getPokemon(
            @Path("id") id: Int,
    ): Pokemon

    @GET
    @Headers("Cacheable: true")
    suspend fun getPokemonSpecies(
            @Url url: String,
    ): PokemonSpecies
}