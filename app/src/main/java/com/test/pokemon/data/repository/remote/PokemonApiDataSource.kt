package com.test.pokemon.data.repository.remote

import com.test.pokemon.data.entity.NamedApiResourceList
import com.test.pokemon.data.entity.Pokemon
import com.test.pokemon.data.entity.PokemonSpecies

class PokemonApiDataSource(private val pokemonApi: PokemonApi) : PokemonApiService {
    override suspend fun getPokemonList(offset: Int, limit: Int): NamedApiResourceList =
        pokemonApi.getPokemonList(offset, limit)

    override suspend fun getNextPokemonList(url: String): NamedApiResourceList =
        pokemonApi.getNextPokemonList(url)

    override suspend fun getPokemon(url: String): Pokemon = pokemonApi.getPokemon(url)

    override suspend fun getPokemon(id: Int): Pokemon = pokemonApi.getPokemon(id)

    override suspend fun getPokemonSpecies(url: String): PokemonSpecies =
            pokemonApi.getPokemonSpecies(url)
}