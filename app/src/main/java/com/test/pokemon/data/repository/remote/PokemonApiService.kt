package com.test.pokemon.data.repository.remote

import com.test.pokemon.data.entity.NamedApiResourceList
import com.test.pokemon.data.entity.Pokemon
import com.test.pokemon.data.entity.PokemonSpecies

interface PokemonApiService {

    suspend fun getPokemonList(offset: Int, limit: Int): NamedApiResourceList

    suspend fun getNextPokemonList(url: String): NamedApiResourceList

    suspend fun getPokemon(url: String): Pokemon

    suspend fun getPokemon(id: Int): Pokemon

    suspend fun getPokemonSpecies(url: String): PokemonSpecies
}