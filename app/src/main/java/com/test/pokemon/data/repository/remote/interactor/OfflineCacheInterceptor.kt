package com.test.pokemon.data.repository.remote.interactor

import com.test.pokemon.utils.Constants
import com.test.pokemon.utils.NetworkUtils
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit

class OfflineCacheInterceptor(private val networkUtils: NetworkUtils) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val header = request.header("Cacheable")

        if (header != null) {
            if (header == "true" && !networkUtils.isConnected()) {
                val cacheControl = CacheControl.Builder()
                    .maxStale(7, TimeUnit.DAYS)
                    .build()

                request = request.newBuilder()
                    .removeHeader(Constants.HEADER_PRAGMA)
                    .removeHeader(Constants.HEADER_CACHE_CONTROL)
                    .cacheControl(cacheControl)
                    .build()
            }
        }
        return chain.proceed(request)
    }
}