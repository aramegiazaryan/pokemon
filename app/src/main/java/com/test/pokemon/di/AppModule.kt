package com.test.pokemon.di

import com.test.pokemon.data.repository.Repository
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModule : BaseModule() {

    override fun get(): Module = module {
        single {
            Repository(pokemonApiDataSource = get())
        }
    }
}
