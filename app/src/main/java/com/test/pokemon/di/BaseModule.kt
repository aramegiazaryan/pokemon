package com.test.pokemon.di

import org.koin.core.module.Module

abstract class BaseModule {

    abstract fun get(): Module
}