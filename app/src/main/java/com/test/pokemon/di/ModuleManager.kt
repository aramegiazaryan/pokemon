package com.test.pokemon.di

import org.koin.core.module.Module

object ModuleManager {
    private val moduleList = arrayOf(
            AppModule,
            NetworkModule,
            UseCaseModule,
            PokemonListModule,
            PokemonDetailsModule
    )

    val modules: List<Module>
        get() {
            return moduleList.map { it.get() }
        }
}