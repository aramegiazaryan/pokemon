package com.test.pokemon.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.test.pokemon.data.repository.remote.ApiFactory
import com.test.pokemon.data.repository.remote.PokemonApi
import com.test.pokemon.data.repository.remote.PokemonApiDataSource
import com.test.pokemon.data.repository.remote.interactor.OfflineCacheInterceptor
import com.test.pokemon.utils.Constants
import com.test.pokemon.utils.NetworkUtils
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule : BaseModule() {
    override fun get(): Module = module {
        single {
           NetworkUtils(androidContext())
        }

        single {
            PokemonApiDataSource(pokemonApi = get())
        }

        single {
            OkHttpClient().newBuilder()
                .cache(Cache(androidContext().cacheDir, Constants.CACHE_SIZE))
                .addInterceptor(OfflineCacheInterceptor(networkUtils = get()))
                .build()
        }

        single {
            ApiFactory.create<PokemonApi>(Constants.BASE_URL_POKEMON_API, get()) {
                it.addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().apply {
                            setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                        }.create()
                    )
                )
            }
        }
    }
}