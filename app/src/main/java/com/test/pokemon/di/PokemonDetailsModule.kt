package com.test.pokemon.di

import com.test.pokemon.ui.details.PokemonDetailsViewModel
import com.test.pokemon.utils.Constants
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object PokemonDetailsModule : BaseModule() {
    override fun get(): Module = module {
        viewModel { (id: Int) ->
            PokemonDetailsViewModel(id, getPokemonDetailsUseCase = get())
        }
    }
}