package com.test.pokemon.di

import com.test.pokemon.ui.list.adapter.PokemonEnergyCheckAdapter
import com.test.pokemon.ui.list.adapter.PokemonListAdapter
import com.test.pokemon.ui.list.PokemonListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object PokemonListModule : BaseModule() {
    override fun get(): Module = module {
        viewModel {
            PokemonListViewModel(
                    application = get(),
                    getPokemonListUseCase = get(),
                    PokemonListAdapter(),
                    PokemonEnergyCheckAdapter()
            )
        }
    }
}