package com.test.pokemon.di

import com.test.pokemon.usecase.GetPokemonDetailsUseCase
import com.test.pokemon.usecase.GetPokemonListUseCase
import org.koin.core.module.Module
import org.koin.dsl.module

object UseCaseModule : BaseModule() {

    override fun get(): Module = module {
        factory {
            GetPokemonListUseCase()
        }

        factory {
            GetPokemonDetailsUseCase()
        }
    }

}
