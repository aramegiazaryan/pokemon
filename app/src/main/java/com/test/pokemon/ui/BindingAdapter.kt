package com.test.pokemon.ui

import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.test.pokemon.R
import com.test.pokemon.ui.list.adapter.PokemonListAdapter

object BindingAdapter {
    @JvmStatic
    @BindingAdapter("bind:init_recyclerview")
    fun initRecyclerView(view: RecyclerView, adapter: PokemonListAdapter) {
        view.adapter = adapter
    }

    @JvmStatic
    @BindingAdapter("bind:load_image")
    fun loadImage(view: AppCompatImageView, url: String?) {
        url?.let {
            view.load(it) {
                placeholder(R.drawable.placeholder)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bind:recyclerview_scroll_end_listener")
    fun recyclerViewScrollEnd(recyclerView: RecyclerView, listener: () -> Unit) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                    listener.invoke()
                }
            }
        })
    }

    @JvmStatic
    @BindingAdapter("bind:recyclerview_scroll_to_top")
    fun recyclerViewScrollToTop(recyclerView: RecyclerView, isScroll: Boolean) {
        if (isScroll) {
            recyclerView.layoutManager?.scrollToPosition(0)
        }
    }

    @JvmStatic
    @BindingAdapter("bind:set_pokemon_types")
    fun setPokemonTypes(linearLayout: LinearLayout, pokemonTypes: List<String>?) {
       pokemonTypes?.run {
           forEach { type ->
               linearLayout.addView(TextView(linearLayout.context).apply {
                   setTextColor(ContextCompat.getColor(context, R.color.black));
                   text = type
               })
           }
       }
    }
}