package com.test.pokemon.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.pokemon.usecase.GetPokemonDetailsUseCase
import com.test.pokemon.usecase.model.PokemonDetails
import kotlinx.coroutines.launch

class PokemonDetailsViewModel(
        pokemonId: Int,
        getPokemonDetailsUseCase: GetPokemonDetailsUseCase
) : ViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val message = MutableLiveData<String>()
    val pokemonDetails = MutableLiveData<PokemonDetails>()

    init {
        viewModelScope.launch {
            try {
                isLoading.value = true

                pokemonDetails.value = getPokemonDetailsUseCase.execute(pokemonId)
            } catch (e: Exception) {
                e.message?.let {
                    message.value = it
                }
            } finally {
                isLoading.value = false
            }
        }
    }
}