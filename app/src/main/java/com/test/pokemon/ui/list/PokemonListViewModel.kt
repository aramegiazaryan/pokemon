package com.test.pokemon.ui.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.test.pokemon.R
import com.test.pokemon.ui.list.adapter.PokemonEnergyCheckAdapter
import com.test.pokemon.ui.list.adapter.PokemonListAdapter
import com.test.pokemon.ui.list.model.PokemonListItemModel
import com.test.pokemon.usecase.GetPokemonListUseCase
import com.test.pokemon.usecase.model.GetPokemonListUseCaseDTO
import com.test.pokemon.usecase.model.PokemonInfo
import com.test.pokemon.utils.Constants
import com.test.pokemon.utils.SingleLiveData
import kotlinx.coroutines.launch
import kotlin.random.Random

class PokemonListViewModel(
        application: Application,
        private val getPokemonListUseCase: GetPokemonListUseCase,
        val pokemonListAdapter: PokemonListAdapter,
        val pokemonEnergyCheckAdapter: PokemonEnergyCheckAdapter
) : AndroidViewModel(application) {
    private val pokemonList = mutableListOf<PokemonInfo>()

    private var allPokemonCount: Int = 0
    private var nextUrl: String? = null

    val isLoading = MutableLiveData<Boolean>()
    val scrollEndListener = MutableLiveData<() -> Unit>()
    val message = MutableLiveData<String>()
    val scrollToTop = MutableLiveData<Boolean>()
    val pokemonId = SingleLiveData<Int>()

    init {
        initialLoading()

        scrollEndListener.value = {
            if (isLoading.value == false) {
                if (nextUrl != null) {
                    loadPokemonItems(false,
                            GetPokemonListUseCaseDTO(scope = viewModelScope, nextUrl = nextUrl)
                    )
                } else {
                    message.value = application.resources.getString(R.string.all_pokemons_loaded)
                }
            }
        }

        pokemonEnergyCheckAdapter.onCheckedChanged = {
            if (isLoading.value == false) {
                pokemonListAdapter.setPokemonListItemModel(getPokemonListItemModel())

                scrollToTop.value = true
            }
        }

        pokemonListAdapter.clickListener {
            pokemonId.value = it.id
        }
    }

    fun onRandomLoad() {
        if (allPokemonCount > 0 && isLoading.value == false) {
            val offset = Random.nextInt(allPokemonCount - Constants.POKEMON_LIST_LIMIT)

            loadPokemonItems(true,
                    GetPokemonListUseCaseDTO(scope = viewModelScope, offset = offset)
            )

            scrollToTop.value = true
        }
    }

    fun initialLoading() = loadPokemonItems(true,
            GetPokemonListUseCaseDTO(scope = viewModelScope))

    private fun loadPokemonItems(isUpdateList: Boolean, dto: GetPokemonListUseCaseDTO) =
            viewModelScope.launch {
                try {
                    isLoading.value = true

                    getPokemonListUseCase.execute(dto).run {
                        if (isUpdateList) {
                            pokemonList.clear()
                        }

                        pokemonList.addAll(pokemonInfoList)
                        this@PokemonListViewModel.allPokemonCount = allPokemonCount
                        this@PokemonListViewModel.nextUrl = nextUrl

                        pokemonListAdapter.setPokemonListItemModel(getPokemonListItemModel())
                    }
                } catch (e: Exception) {
                    e.message?.let {
                        message.value = it
                    }
                } finally {
                    isLoading.value = false
                }
            }

    private fun getPokemonListItemModel(): PokemonListItemModel {
        return if (pokemonEnergyCheckAdapter.getCheckedProperties().isNotEmpty()) {
            val propertyList = pokemonEnergyCheckAdapter.getCheckedProperties()
            val sortedList = pokemonList.sortedBy { pokemonListItemModel ->
                pokemonListItemModel.pokemonEnergy.getEnergy(propertyList)
            }.asReversed()

            val winnerList = mutableListOf<PokemonInfo>().apply {
                add(sortedList[0])
            }
            for (i in 1 until sortedList.size) {
                if (sortedList[0].pokemonEnergy.getEnergy(propertyList) <=
                        sortedList[i + 1].pokemonEnergy.getEnergy(propertyList)) {
                    winnerList.add(sortedList[i])
                } else {
                    break
                }
            }

            val resultList = mutableListOf<PokemonInfo>()
            resultList.addAll(pokemonList)
            winnerList.forEach {
                resultList.remove(it)
            }
            resultList.addAll(0, winnerList)

            PokemonListItemModel(winnerList.size, resultList)
        } else {
            PokemonListItemModel(0, pokemonList)
        }
    }
}