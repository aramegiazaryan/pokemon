package com.test.pokemon.ui.list.adapter

import androidx.lifecycle.MutableLiveData
import com.test.pokemon.usecase.model.PokemonEnergy
import kotlin.reflect.KProperty1

class PokemonEnergyCheckAdapter {
    val hpProperty = PokemonEnergy::hp
    val attackProperty = PokemonEnergy::attack
    val defenseProperty = PokemonEnergy::defense

    val hpChecked = MutableLiveData<Boolean>()
    val attackChecked = MutableLiveData<Boolean>()
    val defenseChecked = MutableLiveData<Boolean>()

    private val checkedProperties = mutableListOf<KProperty1<PokemonEnergy, Int>>()

    var onCheckedChanged: (() -> Unit)? = null

    fun check(property: KProperty1<PokemonEnergy, Int>, data: MutableLiveData<Boolean>) {
        if (data.value != true) {
            data.value = true
            checkedProperties.add(property)
        } else {
            data.value = false
            checkedProperties.remove(property)
        }

        onCheckedChanged?.invoke()
    }

    fun getCheckedProperties() = checkedProperties.toList()
}