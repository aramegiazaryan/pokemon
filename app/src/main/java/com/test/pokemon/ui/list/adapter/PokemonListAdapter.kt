package com.test.pokemon.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.pokemon.databinding.PokemonListItemBinding
import com.test.pokemon.ui.list.model.PokemonListItemModel
import com.test.pokemon.usecase.model.PokemonInfo

class PokemonListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private val pokemonList = mutableListOf<PokemonInfo>()
    private var winnerCount = 0

    private var clickEvent: ((PokemonInfo) -> Unit)? = null

    init {
        setHasStableIds(true)
    }

    fun clickListener(clickEvent: (PokemonInfo) -> Unit) {
        this.clickEvent = clickEvent
    }

    fun setPokemonListItemModel(pokemonListItemModel: PokemonListItemModel) {
        pokemonList.clear()
        pokemonList.addAll(pokemonListItemModel.pokemonInfoList)
        this.winnerCount = pokemonListItemModel.winnerCount
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            PokemonListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(binding, clickEvent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PostViewHolder).bind(pokemonList[position], position <= winnerCount - 1 )
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }

    override fun getItemId(position: Int): Long {
        return pokemonList[position].id.toLong()
    }

    class PostViewHolder(
        private val binding: PokemonListItemBinding,
        private val clickEvent: ((PokemonInfo) -> Unit)?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(pokemonItem: PokemonInfo, isWinner: Boolean) {
            binding.pokemonItem = pokemonItem
            binding.isWinner = isWinner
            binding.root.setOnClickListener {
                clickEvent?.invoke(pokemonItem)
            }
        }

    }
}