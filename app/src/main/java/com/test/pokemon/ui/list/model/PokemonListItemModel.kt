package com.test.pokemon.ui.list.model

import com.test.pokemon.usecase.model.PokemonInfo

data class PokemonListItemModel(
        val winnerCount: Int,
        val pokemonInfoList: List<PokemonInfo>
)
