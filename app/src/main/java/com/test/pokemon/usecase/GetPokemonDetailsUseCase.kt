package com.test.pokemon.usecase

import com.test.pokemon.usecase.model.PokemonDetails

class GetPokemonDetailsUseCase : UseCase<Int, PokemonDetails>() {
    override suspend fun execute(i: Int): PokemonDetails {
        val pokemon = repository.getPokemon(i)

        val pokemonSpecies = repository.getPokemonSpecies(pokemon.species.url)

        return pokemonToPokemonDetailsMapper(pokemon, pokemonSpecies)
    }
}