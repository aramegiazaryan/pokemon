package com.test.pokemon.usecase

import com.test.pokemon.usecase.model.GetPokemonListUseCaseDTO
import com.test.pokemon.usecase.model.PokemonInfo
import com.test.pokemon.usecase.model.PokemonModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch

class GetPokemonListUseCase : UseCase<GetPokemonListUseCaseDTO, PokemonModel>() {
    override suspend fun execute(i: GetPokemonListUseCaseDTO): PokemonModel = i.run {
        var allPokemonCount: Int
        var url: String?

        val jobs: MutableList<Deferred<PokemonInfo>> = mutableListOf()

        val apiResult = if (nextUrl == null) repository.getPokemonList(offset, limit)
        else repository.getNextPokemonList(nextUrl)
        apiResult.run {
            allPokemonCount = count
            url = next

            results.map {
                scope.launch {
                    async {
                        pokemonToPokemonInfoMapper(repository.getPokemon(it.url))
                    }.let {
                        jobs.add(it)
                    }
                }
            }
        }

        PokemonModel(
                allPokemonCount,
                url,
                jobs.awaitAll()
        )
    }
}