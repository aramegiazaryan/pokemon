package com.test.pokemon.usecase

import com.test.pokemon.data.repository.Repository
import org.koin.java.KoinJavaComponent.inject


abstract class UseCase<I, O> {
    val repository by inject(Repository::class.java)

    abstract suspend fun execute(i: I): O
}

