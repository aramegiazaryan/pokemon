package com.test.pokemon.usecase.model

import com.test.pokemon.utils.Constants
import kotlinx.coroutines.CoroutineScope

data class GetPokemonListUseCaseDTO(
        val scope: CoroutineScope,
        val offset: Int = 0,
        val limit: Int = Constants.POKEMON_LIST_LIMIT,
        val nextUrl: String? = null,
)
