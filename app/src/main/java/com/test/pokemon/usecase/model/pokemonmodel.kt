package com.test.pokemon.usecase.model

import kotlin.reflect.KProperty1

data class PokemonModel(
    val allPokemonCount: Int,
    val nextUrl: String?,
    val pokemonInfoList: List<PokemonInfo>
)

data class PokemonInfo(
        val id: Int,
        val name: String,
        val thumbnail: String?,
        val pokemonEnergy: PokemonEnergy
)

data class PokemonEnergy(
        val hp: Int,
        val attack: Int,
        val defense: Int
) {
    fun getEnergy(propertyList: List<KProperty1<PokemonEnergy, Int>>): Int {
        var sum = 0
        propertyList.forEach {
            sum += it.get(this)
        }

        return sum / propertyList.size
    }
}

data class PokemonDetails(
        val id: Int,
        val name: String,
        val description: String,
        val image: String?,
        val height: Int,
        val weight: Int,
        val color: String,
        val types: List<String>,
        val pokemonEnergy: PokemonEnergy
)
