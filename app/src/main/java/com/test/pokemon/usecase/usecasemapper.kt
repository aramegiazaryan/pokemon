package com.test.pokemon.usecase

import com.test.pokemon.data.entity.*
import com.test.pokemon.usecase.model.PokemonDetails
import com.test.pokemon.usecase.model.PokemonEnergy
import com.test.pokemon.usecase.model.PokemonInfo
import com.test.pokemon.utils.Constants
import java.util.*

internal fun pokemonToPokemonInfoMapper(pokemon: Pokemon): PokemonInfo {
    return pokemon.run {
        PokemonInfo(
            id,
            name.capitalize(Locale.ROOT),
            sprites.frontDefault,
            statsToPokemonEnergyMapper(stats)
        )
    }
}

internal fun statsToPokemonEnergyMapper(stats: List<PokemonStat>): PokemonEnergy {
    return stats.let {
        val map = it.map { stat -> stat.stat.name to stat.baseStat }.toMap()
        PokemonEnergy(
            map.getOrDefault(Constants.HP_KEY, 0),
            map.getOrDefault(Constants.ATTACK_KEY, 0),
            map.getOrDefault(Constants.DEFENSE_KEY, 0)
        )
    }
}

internal fun pokemonToPokemonDetailsMapper(
    pokemon: Pokemon,
    pokemonSpecies: PokemonSpecies
): PokemonDetails {
    return pokemon.run {
        PokemonDetails(
            id,
            name.capitalize(Locale.ROOT),
            flavorTextEntriesToDescription(pokemonSpecies.flavorTextEntries),
            sprites.other?.officialArtwork?.frontDefault,
            height,
            weight,
            pokemonSpecies.color.name.capitalize(Locale.ROOT),
            pokemonTypesToTypes(types),
            statsToPokemonEnergyMapper(stats)
        )
    }
}

internal fun pokemonTypesToTypes(pokemonTypes: List<PokemonType>): List<String> = pokemonTypes.map {
    it.type.name.capitalize(Locale.ROOT)
}

internal fun flavorTextEntriesToDescription(flavorTextEntries: List<PokemonSpeciesFlavorText>): String {
    val description = StringBuilder()
    flavorTextEntries.forEach {
        if (it.language.name == Constants.DEFAULT_LANG) {
            description.append(it.flavorText.replace("\n", ""))
        }
    }

    return description.toString()
}
