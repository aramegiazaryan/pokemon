package com.test.pokemon.utils

object Constants {
    const val DEFAULT_LANG = "en"

    const val BASE_URL_POKEMON_API = "https://pokeapi.co/api/v2/"

    const val CACHE_SIZE = 20 * 1024 * 1024.toLong()
    const val HEADER_CACHE_CONTROL = "Cache-Control"
    const val HEADER_PRAGMA = "Pragma"

    const val POKEMON_LIST_LIMIT = 30

    const val HP_KEY = "hp"
    const val ATTACK_KEY = "attack"
    const val DEFENSE_KEY = "defense"
}