package com.test.pokemon.utils

import android.content.Context
import android.net.ConnectivityManager

class NetworkUtils(private val context: Context) {
    fun isConnected(): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connMgr.activeNetwork != null
    }
}